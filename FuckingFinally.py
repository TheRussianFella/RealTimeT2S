from fractions import Fraction
import numpy as np

class Logoot:
    def __init__(self, str = ""):
        self.data = {};
        self.data[Fraction(0)] = ('0', False);
        self.data[Fraction(1)] = ('\0', True);

        length = len(str);
        for i in range(length):
            self.data[Fraction(i + 1, length + 1)] = (str[i], True);

    def insert_by_coord(self, ind, atom):
        keys = sorted(list(self.data.keys()));
        
        new_ind = (ind + keys[keys.index(ind) - 1])/2;
        
        self.data[new_ind] = (atom, True);

    def delete_by_coord(self, ind):
        (atom, flag) = self.data[ind];
        self.data[ind] = (atom, False);

    def __str__(self):
        string = "";
        keys = sorted(self.data.keys());
        for i in keys:
            (atom, flag) = self.data[i];
            if flag: string += atom;
        return string;

    def n_to_coord(self, num):
        keys = sorted(list(self.data.keys()));

        j = 0;
        for i in range(len(keys)):
            (atom, flag) = self.data[keys[i]];
            if flag:
                if j == num:
                    return keys[i];
                j += 1

        return Fraction(1);
        
    def insert(self, index, atom):
        self.insert_by_coord(self.n_to_coord(index), atom);
        
    def delete(self, index):
        self.delete_by_coord(self.n_to_coord(index));

import Bio
from Bio import pairwise2

def diff_files(original, changed):

    if len(original) == 0:
        return [('ins', i, x) for i, x in enumerate(changed)]

    alignments = pairwise2.align.globalxx(original, changed)[0];
    alignmentOr = alignments[0];
    alignmentCh = alignments[1];
    
    print(alignmentOr);
    print(alignmentCh);
    
    commands = [];
    j = 0;
    for i in range(len(alignmentOr)):
        if alignmentOr[i] == alignmentCh[i]:
            j += 1;
        elif alignmentOr[i] == '-': 
            commands.append(("ins", j, alignmentCh[i]));
            j += 1
        elif alignmentCh[i] == '-': 
            commands.append(("del", j));
        
    return commands;

def restore_file(file, patches):
    if not patches: return file;
    
    data = Logoot(file);
    for command in patches:
        if command[0] == "del":
            data.delete(command[1]);
        elif command[0] == "ins":
            data.insert(command[1], command[2]);
        else:
            raise "Holy shit unrecognised command";
    
    return str(data);