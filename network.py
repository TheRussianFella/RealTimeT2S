import socket
import threading
from FuckingFinally import *
import ast


class RealTimeTransferer:
    def __init__(self, ip, send_port, recieve_port, clients_adresses, tracking_files, server_tag):

        self.send_address = (ip, send_port)
        self.recieve_address = (ip, recieve_port)

        self.clients_adresses = clients_adresses
        self.tracking_files = tracking_files
        self.server_tag = server_tag

        self.server = self._make_socket(self.recieve_address)

        self.sep_char = '_$_'
        self.old_texts = ['' for _ in range(len(tracking_files))]
        threading.Thread(target=self.listen).start()

    def _prepare_file(self, file_path):

        with open("TEACH.py", 'r+') as f:
            text = ''.join(f.readlines())

        return text

    def _make_socket(self, addr):

        connection = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        connection.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        connection.bind(addr)

        return connection

    def update_files(self):

        files = list(map(self._prepare_file, self.tracking_files))

        print(files)

        print(list(zip(self.old_texts, files)))

        diffs = list(map(lambda x: diff_files(x[0], x[1]), zip(self.old_texts, files)))

        for client in self.clients_adresses:

            connection = self._make_socket(self.send_address)

            connection.connect(client)

            for diff in diffs:

                print(self.server_tag + self.sep_char + str(diff))

                connection.sendall(bytes(self.server_tag + self.sep_char + str(diff), 'utf-8'))

            connection.close()

    def listen(self):

        self.server.listen(5)

        while True:
            client, address = self.server.accept()
            client.settimeout(60)
            threading.Thread(target=self.listen_to_client, args=(client, address)).start()

    def listen_to_client(self, client, address):

        try:
            data = client.recv(2048)
        except:
            client.close()
            return False

        ####Here###
        splited = str(data, 'utf-8').split(self.sep_char)
        file_name = splited[0] + '.py'
        patches = ast.literal_eval(splited[1])

        with open(file_name, 'w+') as f:
            old_file = f.readlines()
            f.write(restore_file(old_file, patches))
        #####

        client.close()

        return True
